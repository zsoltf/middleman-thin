# Middleman-thin

Yet another [Middleman] template with Bower, Bourbon, Bitters and Neat.

Middleman is an excellent static site generator with many amazing features.

This minimalist template extends the default one with Bower for managing packages and
Bourbon niceness.

## Install

### Prerequisites

Ruby

Node

Middleman

      gem install middleman

Bower

      npm install -g bower

### Template

#### One time use

Clone repo

      git clone https://github.com/zsoltf/middleman-thin.git myproject

Remove .git folder and init your own

      cd myproject
      rm -rf .git/
      git init

Install Bower packages

      bower install

Launch middleman dev server

      middleman

#### Install as a template

Clone to ~/.middleman/

      git clone https://github.com/zsoltf/middleman-thin.git ~/.middleman/thin

Init new project with template

      middleman init my-project -T=thin
      cd my-project
      bower install
      middleman

Start hacking!


  [middleman]: http://middlemanapp.com/
